1) Used database - MariaDB (compatible with MySql)
  # scripts for DB and tables creation are in createBase.sql in root folder, - should be launched from MySQL / MariaDB commandline

2) Server-side was written with NodeJs 6.x using ExpressJs 4.x framework and mariadb connector. Defaultly runs on 8080 port
  # Command for launching:> node server.js.

3) Client-side built with React 16.x / Redux using ES6-Next features and CRA utility.
  - CRA application is not ejected, which incapsulates all boilerplate configs from one    side, and increases difficulty level for working with preprocessors and tests from     another.
  - Used SCSS preprocessor, Bootstrap 4 for responsive designing.
  - Written some basic unit tests for main components using Jest/Enzyme

  # npm start
    - running app in dev mode on localhost:3000
  # npm build
    - CRA builds ready-for-prod version, which keeps in 'build' folder and runs on port    5000 by default
  # npm test
    - running available tests
