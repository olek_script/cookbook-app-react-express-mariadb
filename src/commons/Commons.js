export function prepareDate (created) {
  if (!created) { return }

  let date = new Date(created)
  return `${date.toLocaleDateString()}, ${date.toLocaleTimeString()}`
}