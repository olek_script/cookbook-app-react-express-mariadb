import * as types from './actionTypes'
import serverAPI from '../api/serverAPI'

export function createRecipe (params) {
  return function (dispatch) {
    return serverAPI.addNewRecipe(params)
      .then((response) => {
        dispatch({ type: types.UPDATE_RECIPES_ALTERATION_HISTORY, message: response.message})
      }) 
  }
}

export function updateRecipe (recipe) {
  return function (dispatch) {
    return serverAPI.updateRecipe(recipe)
      .then((response) => {
        dispatch({ type: types.UPDATE_RECIPES_ALTERATION_HISTORY, message: response.message})
      }) 
  }
}

export function deleteRecipe (recipe) {
  return function (dispatch) {
    return serverAPI.deleteRecipesGroup(recipe)
      .then((response) => {
        dispatch({ type: types.UPDATE_RECIPES_ALTERATION_HISTORY, message: response.message})
      }) 
  }
}
