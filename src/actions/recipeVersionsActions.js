import * as types from './actionTypes'
import serverAPI from '../api/serverAPI'

export function loadRecipeVersions (groupId) {
  return function (dispatch) {
    return serverAPI.getAllRecipeVersions(groupId)
      .then((recipeVersions) => {
        dispatch({ type: types.LOAD_RECIPE_VERSIONS, recipeVersions })
      })
  }
}
