import * as types from './actionTypes'
import serverAPI from '../api/serverAPI'

export function loadRecipesList () {
  return function (dispatch) {
    return serverAPI.getAllRecipes()
      .then((recipes) => {
        dispatch({ type: types.LOAD_RECIPES_LIST, recipes })
      })
  }
}
