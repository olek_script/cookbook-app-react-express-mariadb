import * as types from './actionTypes'
import serverAPI from '../api/serverAPI'

export function loadRecipeById (id) {
  return function (dispatch) {
    return serverAPI.getRecipe(id)
      .then((recipes) => {
        dispatch({ type: types.LOAD_SINGLE_RECIPE, recipe: recipes[0] })
      })
  }
}

export function clearCurrentRecipe () {
  return { type: types.CLEAR_CURRENT_RECIPE }
}
