import React from 'react'
import configureStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { shallow } from 'enzyme'

import RecipeFormPage from '../../Components/RecipeFormPage/RecipeFormPage';

jest.dontMock('../../Components/RecipeFormPage/RecipeFormPage')

describe('RecipeFormPage Component ', () => {
 
  it("displays under 'recipe/update' route,", () => {
    let middlewares = [thunk]
    const mockStore = configureStore(middlewares);
    const initialState = {
      currentRecipe: {
        title: 'Mock recipe 1',
        description: 'Description 1',
        id: 'id 1',
      }
    }
    let store = mockStore(initialState);
    let location = { search: '?id=4EAF0CB7B85D11E892230013D42B8086' } 
    const match = { params: { action: 'update' } } 
    const wrapper = shallow(
      <RecipeFormPage match={match} store={store} location={location}/>
    ).dive();
   
    expect(wrapper.contains(<h3 className="page-title">Update existing recipe</h3>)).toBe(true)
  });

  it("displays under 'recipe/create' route,", () => {
    let middlewares = [thunk]
    const mockStore = configureStore(middlewares);
    const initialState = {
      currentRecipe: {
        title: 'Mock recipe 1',
        description: 'Description 1',
        id: 'id 1',
      }
    }
    let store = mockStore(initialState);
    let location = { search: '' } 
    const match = { params: { action: 'create' } } 
    const wrapper = shallow(
      <RecipeFormPage match={match} store={store} location={location}/>
    ).dive();
   
    expect(wrapper.contains(<h3 className="page-title">Create brand new recipe</h3>)).toBe(true)
  });

})