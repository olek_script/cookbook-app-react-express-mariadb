import React from 'react'
import { Provider } from 'react-redux'
import { MemoryRouter } from 'react-router'

import configureStore from 'redux-mock-store'
import { shallow } from 'enzyme'

import App from '../App';

jest.dontMock('../App')

describe('App Component ', () => {
  it('renders without crashing,', () => {
    shallow(<App />);
  });

  it('correctly routes to RecipeDetails,', () => {
    const mockStore = configureStore();
    const initialState = {
      currentRecipe: {
        title: 'Mock recipe',
        groupId: '123',
      },
      currentRecipeVersions: [],
      recipeAlterationVersions: []
    }
    let store = mockStore(initialState);
    const wrapper = shallow(
      <MemoryRouter initialEntries={['/details']} initialIndex={0}>
        <Provider store={store}>
          <App />
        </Provider> 
      </MemoryRouter>
    );
   
    expect(wrapper.html()).toContain('Mock recipe details page');
  });

  it('correctly routes to RecipesList,', () => {
    const mockStore = configureStore();
    const initialState = {
      recipeItems: [{
        title: 'Mock title 1',
        description: 'Mock description 1',
        version: 1
      }, {
        title: 'Mock title 2',
        description: 'Mock description 2',
        version: 2
      }]
    }
    let store = mockStore(initialState);
    const wrapper = shallow(
      <MemoryRouter initialEntries={['/']} initialIndex={0}>
        <Provider store={store}>
          <App />
        </Provider> 
      </MemoryRouter>
    );
   
    const html = wrapper.html()
    expect(html).toContain('Mock title 1');
    expect(html).toContain('Mock description 2');
  });

  it('redirects from any unsuitable route to RecipesList,', () => {
    const mockStore = configureStore();
    const initialState = {
      recipeItems: [{
        title: 'Item 1',
        description: 'Mock description 2'
      }]
    }
    let store = mockStore(initialState);

    const wrapper = shallow(
      <MemoryRouter initialEntries={['/unsuitable/route', '/test']} initialIndex={0}>
        <Provider store={store}>
          <App />
        </Provider>
      </MemoryRouter>
    );
    expect(wrapper.html()).toContain('<div class=\"react-redirect\"></div>');
    // wrapper.props().history.push('/')
    // expect(wrapper.html()).toContain('Item 1');  
  }); 

  it('routes to RecipeFormPage if recipe updating / adding,', () => {
    const mockStore = configureStore();
    const initialState = {
      currentRecipe: {
        title: 'Mock title 1'
      }
    }
    let store = mockStore(initialState);
    const wrapper = shallow(
      <MemoryRouter initialEntries={['/recipe/add', '/recipe/update']} initialIndex={0}>
        <Provider store={store}>
          <App />
        </Provider> 
      </MemoryRouter>
    );

    expect(wrapper.html()).toContain('Recipe title');
    expect(wrapper.html()).toContain('Description');
    expect(wrapper.html()).toContain('Submit request');
  });

})