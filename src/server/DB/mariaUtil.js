const mariadb = require('mariadb');
let config = require('../config')

var dbPool;
module.exports = {
  createDbConnectionPool: function () {
    return dbPool = mariadb.createPool(config.db);
  },
  getDbConnectionInstance: function () {
    return dbPool;
  }
}