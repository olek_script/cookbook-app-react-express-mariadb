module.exports = class Recipes {
  constructor(dbPool){
    this.dbPool = dbPool
  }

  getAllRecipes () {
    return this.dbPool.getConnection()
    .then(conn => {
      return conn.query(`
        SELECT HEX(id) as id, HEX(group_id) as groupId, version, is_actual as isActual, created, title, description 
        FROM recipes 
        WHERE is_actual=true
        GROUP BY created DESC
      `)
      .then((rows) => {
        conn.end();
        return rows;
      })
      .catch((error) => {
        console.log('Error while getting all recipes')
        console.log(error);
        conn.end();
      })
    })
  }

  getRecipe (params) {
    return this.dbPool.getConnection()
    .then(conn => {
      return conn.query(`
        SELECT HEX(id) as id, HEX(group_id) as groupId, version, is_actual as isActual, created, title, description 
        FROM recipes 
        WHERE id=UNHEX(REPLACE(? ,'-',''))
      `, [
        params.id
      ])
        .then((row) => {
          conn.end();
          return row;
        })
        .catch((error) => {
          console.log('Error while getting recipe')
          console.log(error);
          conn.end();
        })
    })
  }

  addNewRecipe (params) {
    return this.dbPool.getConnection()
    .then(conn => {
      return conn.query(`
        INSERT INTO recipes (id, group_id, version, is_actual, created, title, description) 
        VALUES ( UNHEX(REPLACE(UUID(),'-','')) , UNHEX(REPLACE(UUID(),'-','')) , 1, true, NOW(), ?, ? )
      `, [
        params.title,
        params.description
      ])
      .then(() => {
        conn.end();
        return params.title;
      })
      .catch((error) => {
        console.log('Error while adding new recipe')
        console.log(error);
        conn.end();
      })
    })
  }

  updateRecipe (params) {
    return this.dbPool.getConnection()
    .then(conn => {
      return conn.query(`
        UPDATE recipes SET is_actual=false 
        WHERE id = UNHEX(REPLACE(? ,'-',''))
        `, [
        params.id
      ])
      .then(() => {
        return conn.query(`
          INSERT INTO recipes (id, group_id, version, is_actual, created, title, description) 
          VALUES ( UNHEX(REPLACE(UUID(),'-','')) , UNHEX(REPLACE(? ,'-','')) , (? + 1), true, NOW(), ?, ? )
        `, [
          params.groupId,
          params.version,
          params.title,
          params.description
        ])
      })
      .then(() => {
        conn.end();
      })
      .catch((error) => {
        console.log('Error while updating recipe')
        console.log(error);
        conn.end();
      })
    })
  }

  deleteRecipe (params) {
    return this.dbPool.getConnection()
    .then(conn => {
      return conn.query(`
        DELETE FROM recipes WHERE group_id = UNHEX(REPLACE(? ,'-',''))
      `, [
        params.groupId,
      ])
      .then(() => {
        conn.end();
        return params.title;
      })
      .catch((error) => {
        console.log('Error while deleting recipe:')
        console.log(error);
        conn.end();
      })
    })
  }

  getRecipeVersions (params) {
    return this.dbPool.getConnection()
    .then(conn => {
      return conn.query(`
        SELECT HEX(id) as id, HEX(group_id) as groupId, version, is_actual as isActual, created, title, description 
        FROM recipes 
        WHERE group_id = UNHEX(REPLACE(? ,'-','')) AND is_actual = false
        GROUP BY created DESC
      `,[
        params.groupId
      ])
      .then((rows) => {
        conn.end();
        return rows;
      })
      .catch((error) => {
        console.log('Error while getting recipe versions:')
        console.log(error);
        conn.end();
      })
    })
  }

}