const recipes = require('./recipes')

module.exports = function(app, path){
  app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, 'build', 'index.html'));
  });

  recipes(app, path)
}