const mariaUtil = require('../DB/mariaUtil');
const Recipes = require('../DB/Facades/Recipes')
let recipes = new Recipes(mariaUtil.getDbConnectionInstance())

let wrongRequest = function(res, next, errorMessage){
  res.status(400).json({message: errorMessage})
  return next(new Error(errorMessage))
}

module.exports = function(app, path){

  app.get('/api/recipes', function (req, res) {
    recipes.getAllRecipes()
      .then((recipes) => {
        res.json(recipes)
      })
      .catch((error) => {
        console.log(`Failed to get all recipes: ${error}`)
      })
  });

  app.get('/api/recipe/versions', function(req, res, next){
    let query = req.query
    if (query.groupId === false) {
      return wrongRequest(res, next, 'Tried to get recipe versions without groupId')
    }

    recipes.getRecipeVersions(query)
      .then((versions) => {
        res.json(versions)
      })
      .catch((error) => {
        console.log(`Failed to get recipe versions: ${error}`)
      })
  })

  app.get('/api/recipe', function(req, res, next){
    let query = req.query
    if (query.id === undefined) {
      return wrongRequest(res, next, 'Tried to request for recipe without specifying id')
    }

    recipes.getRecipe(query)
      .then((recipes) => {
        res.json(recipes)
      })
      .catch((error) => {
        console.log(`Failed to get recipe: ${error}`)
      })
  })

  app.post('/api/recipes/add', function(req, res, next){
    let requestBody = req.body
    if (!requestBody.title || requestBody.title.length === 0) {
      return wrongRequest(res, next, 'Added recipe has no title')
    }

    recipes.addNewRecipe(requestBody)
      .then((recipeTitle) => {
        res.json({message: `Recipe ${recipeTitle} was created`})
      })
      .catch((error) => {
        console.log(`Failed to add new recipe: ${error}`)
      })
  })

  app.patch('/api/recipes/update', function(req, res, next){
    let requestBody = req.body
    if (!requestBody.id || requestBody.id.length === 0 ) {
      return wrongRequest(res, next, 'Recipe id were not specified')
    } else if (!requestBody.title || !requestBody.previousTitle) {
      return wrongRequest(res, next, 'No recipe title was received')
    } else if (requestBody.isActual !== true) {
      return wrongRequest(res, next, 'Trying to update archived recipe')
    }

    recipes.updateRecipe(requestBody)
      .then(() => {
        res.json({message: `Recipe ${requestBody.title} (${requestBody.previousTitle}) was updated`})
      })
      .catch((error) => {
        console.log(`Failed to update recipe: ${error}`)
      })
  })

  app.delete('/api/recipes/delete', function(req, res, next){
    let requestBody = req.body
    if (requestBody.groupId === undefined) {
      return wrongRequest(res, next, 'GroupId is required for delete recipes')
    } else if (requestBody.isActual !== true) {
      return wrongRequest(res, next, 'Trying to delete archived recipe without parent deletion')
    }

    recipes.deleteRecipe(requestBody)
      .then((recipeTitle) => {
        res.json({message: `Recipe ${recipeTitle} and all its versions were deleted`})
      })
      .catch((error) => {
        console.log(`Failed to delete recipe: ${error}`)
      })
  })
}