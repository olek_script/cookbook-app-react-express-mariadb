module.exports = {
  port: process.env.PORT || 8080,
  buildFolder: 'build',
  allowOriginAddress: process.env.NODE_ENV === 'prod' ? 'http://localhost:5000' : 'http://localhost:3000',
  db: {
    host: '127.0.0.1', 
    user:'root', // admin 
    connectionLimit: 5,
    database: 'cookbook'
    // port: 
    // password:
  }
}