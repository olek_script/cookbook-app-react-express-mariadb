import React, { Component } from 'react';
import { Switch } from 'react-router'
import { BrowserRouter, Route, Redirect } from 'react-router-dom'

import Header from './Components/Header/Header'
import RecipesList from './Components/RecipesList/RecipesList'
import RecipeDetails from './Components/RecipeDetails/RecipeDetails'
import RecipeFormPage from './Components/RecipeFormPage/RecipeFormPage'
import './App.css';

const Fragment = React.Fragment
class App extends Component {
  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <Fragment>
            <Header/>
            <Switch>
              <Route path="/details" component={RecipeDetails} />
              <Route exact path="/recipe/:action" render={(props) => {
                return <RecipeFormPage key={props.match.params.action} {...props}/>
              }} />
              <Route exact path="/" component={RecipesList} />
              <Route path="*" render={() => {
                return <div className="react-redirect"><Redirect to={{pathname: '/details'}} /></div>
              }} />

            </Switch>
          </Fragment>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
