import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Link } from 'react-router-dom'

import * as recipesListActions from '../../actions/recipesListActions'
import { prepareDate } from '../../commons/Commons'
import './RecipesList.css'

class RecipesList extends React.Component {
  state = {
    recipes: []
  }
  isComponentStillMounted = true

  static getDerivedStateFromProps (nextProps, prevState) {
    return {
      recipes: nextProps.recipes || []
    }
  }

  componentDidMount () {
    this.props.actions.loadRecipesList()
  }

  componentWillUnmount () {
    this.isComponentStillMounted = false
  }

  render () {
    const isRecipesListNotEmpty = this.state.recipes.length > 0
    return (
      <div id="recipes-list" className="fade-in-animation">
        <h3 className="page-title">Recipes list</h3>
        {!isRecipesListNotEmpty && <div className="no-data-prompt">Recipes list is empty</div>}
        {isRecipesListNotEmpty && <div className="recipes-container row">
          {this.state.recipes.map((recipe, i) => {
            return <div key={i} className="recipe col-11 col-sm-5 col-md-3 col-lg-2">
              <div className="row recipe-info">
                <span className="recipe-date">{prepareDate(recipe.created)}</span>
                <span className="recipe-version">version: {recipe.version}</span>
              </div>
              <div className="row">
                <img src="images/recipe.png" alt="-"/>
                <span className="recipe-title col-7">{recipe.title}</span>
              </div>
              <div className="recipe-desc">{recipe.description}</div>
              <div className="details-link"><Link to={{pathname: `/details`, search: `&id=${recipe.id}`}}>details...</Link></div>
            </div>
          })}
        </div>}
      </div>
    )
  }
}

RecipesList.propTypes = {
  recipes: PropTypes.array.isRequired
}

function mapStateToProps (storageState, ownProps) {
  return {
    recipes: storageState.recipeItems
  }
}

function mapDispatchToProps (dispatch) {
  return { actions: bindActionCreators(recipesListActions, dispatch) }
}

export default connect(mapStateToProps, mapDispatchToProps)(RecipesList)