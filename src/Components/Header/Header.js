import React from 'react'
import { Link } from 'react-router-dom'
import './Header.css'

const Header = (props) => {
  return (
    <div id="header">
      <h4>Cookbook test app</h4>
      <div className="menu">
        <Link to={{pathname: '/'}}><span className="menu-item recipes-list">Recipes list</span></Link>
        <Link to={{pathname: '/recipe/create'}}><span className="menu-item recipe-new">Add new</span></Link>
      </div>
    </div>
  )
}

export default Header
