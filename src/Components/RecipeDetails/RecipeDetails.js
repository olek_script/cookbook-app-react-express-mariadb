import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import queryString from 'query-string'

import * as currentRecipeActions from '../../actions/currentRecipeActions'
import * as recipeVersionsActions from '../../actions/recipeVersionsActions'
import * as recipesAlterationHistoryActions from '../../actions/recipesAlterationHistoryActions'
import { prepareDate } from '../../commons/Commons'
import './RecipeDetails.css'

class RecipeDetails extends React.Component {
  state = {
    currentRecipe: {},
    currentRecipeVersions: [],
    chosenRecipe: {},
    firstLoad: true,
    currentRecipeExists: false
  }
  isComponentStillMounted = true

  static getDerivedStateFromProps (nextProps, prevState) {
    return {
      currentRecipe: nextProps.currentRecipe || {},
      currentRecipeVersions: nextProps.currentRecipeVersions || [],
      currentRecipeExists: nextProps.currentRecipe.groupId && nextProps.currentRecipe.groupId.length > 0
    }
  }

  onLastVersionClicked = () => {
    this.setState({
      chosenRecipe: {...this.state.currentRecipe}
    })
  }

  onArchivedVersionClicked = (i) => {
    this.setState({
      chosenRecipe: {...this.state.currentRecipeVersions[i]}
    })
  }

  onUpdateRecipe = () => {
    this.props.history.push(`/recipe/update?id=${this.state.currentRecipe.id}`)
  }

  onDeleteRecipe = () => {
    this.props.actions.deleteRecipe({...this.state.currentRecipe})
      .then(() => {
        this.props.history.push('/')
      })
  }

  componentDidMount () {
    let recipeId = queryString.parse(this.props.location.search).id

    this.props.actions.loadRecipeById(recipeId)
    .then(() => {
      this.props.actions.loadRecipeVersions(this.state.currentRecipe.groupId)
      this.isComponentStillMounted && this.setState({
        chosenRecipe: {...this.state.currentRecipe}
      })
    })
  }

  componentWillUnmount () {
    this.props.actions.clearCurrentRecipe()
    this.isComponentStillMounted = false
  }

  render () {
    const isVersions = this.state.currentRecipeVersions.length > 0
    const chosenRecipe = this.state.chosenRecipe

    return (
      <div id="recipe-details" className="fade-in-animation">
        {!this.state.currentRecipeExists && <div className="no-data-prompt">No such recipe</div>}

        {this.state.currentRecipeExists && <div>
          <h3 className="page-title">{`${this.state.currentRecipe.title} details page`}</h3>
          
          <div className="row recipe-panel">
            <div className="recipe-menu col-12 col-sm-5 col-md-4 col-lg-3">

              <div className="recipe-actions">
                <div className="recipe-action update" onClick={this.onUpdateRecipe}>
                  Update current recipe</div>
                <div className="recipe-action delete" onClick={this.onDeleteRecipe}>
                  Delete recipe and all it's versions</div>
              </div>

              <div className="recipe-versions">

                <div className="recipe-versions-separator">Last version</div>
                <div className="recipe-version" onClick={this.onLastVersionClicked}>
                  <span className="recipe-date">Created: {prepareDate(this.state.currentRecipe.created)}</span>
                  <span className="recipe-version-number">v{this.state.currentRecipe.version}</span>
                </div>

                {isVersions && <div className="recipe-versions-separator">Previous versions</div>}
                {isVersions && this.state.currentRecipeVersions.map((item, i) => {
                  return <div key={i} onClick={this.onArchivedVersionClicked.bind(this, i)} 
                    className="recipe-version">
                    <span className="recipe-date">Created: {prepareDate(item.created)}</span>
                    <span className="recipe-version-number">v{item.version}</span>
                  </div>
                })}
              </div>

            </div>

            <div className="recipe-view col-12 col-sm-7 col-md-8m col-lg-9">
              <h3>{chosenRecipe.title}{chosenRecipe && chosenRecipe.isActual === false && <span> (archived)</span>}</h3>
              <div>{chosenRecipe.description}</div>
            </div>

          </div>
        </div>}
      </div>
    )
  }
}

RecipeDetails.propTypes = {
  currentRecipe: PropTypes.object.isRequired,
  currentRecipeVersions: PropTypes.array.isRequired,
  history: PropTypes.object.isRequired,
  location: PropTypes.shape({
    search: PropTypes.string.isRequired
  })
}

function mapStateToProps (storageState, ownProps) {
  return {
    currentRecipe: storageState.currentRecipe,
    currentRecipeVersions: storageState.currentRecipeVersions,
  }
}

function mapDispatchToProps (dispatch) {
  return { actions: bindActionCreators({
    ...currentRecipeActions,
    ...recipeVersionsActions,
    ...recipesAlterationHistoryActions
  }, dispatch) }
}

export default connect(mapStateToProps, mapDispatchToProps)(RecipeDetails)