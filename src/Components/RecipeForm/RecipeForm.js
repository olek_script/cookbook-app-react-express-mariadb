import React from 'react'
import PropTypes from 'prop-types'
import { Field, reduxForm } from 'redux-form'

import './RecipeForm.css'

class RecipeForm extends React.Component {
  componentDidMount = () => {
    this.props.initialize({ 
      title: this.props.currentRecipe.title,
      description: this.props.currentRecipe.description
    })
  }

  render = () => {
    return (
      <div id="recipe-form" className="fade-in-animation">
        <form onSubmit={this.props.handleSubmit}>
          <span className="range-separator">Recipe title</span>
          <Field name="title"
            className="form-control" component="input" type="text"
          />
          <span className="range-separator">Description</span>
          <Field name="description"
            className="form-control" component="textarea" type="text"
          />
          <div className="buttons">
            <button type="submit" className="btn btn-search btn-secondary btn-sm">
              <span>Submit request</span>
            </button>
          </div>
        </form>
      </div>
    )
  }
}

RecipeForm.propTypes = {
  currentRecipe: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func.isRequired,

  onSubmit: PropTypes.func.isRequired,
}

export default reduxForm({
  form: 'recipe'
})(RecipeForm)
