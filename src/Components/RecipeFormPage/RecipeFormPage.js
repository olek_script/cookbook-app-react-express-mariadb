import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import queryString from 'query-string'

import RecipeForm from '../RecipeForm/RecipeForm'
import * as currentRecipeActions from '../../actions/currentRecipeActions'
import * as recipesAlterationHistoryActions from '../../actions/recipesAlterationHistoryActions'
import './RecipeFormPage.css'

class RecipeFormPage extends React.Component {
  state = {
    currentRecipe: {},
    recipeActionType: null,
    isUserFormInvalid: false,
    isRequestedPageValid: true,
    pageTitle: ''
  }

  static getDerivedStateFromProps (nextProps, prevState) {
    return {
      currentRecipe: nextProps.currentRecipe || {},
      recipeActionType: nextProps.match.params.action
    }
  }

  onFormSubmit = (params) => {
    if (!params.title || params.title.length < 1) {
      return this.setState({ isUserFormInvalid: true })
    }

    params = {...params}
    Object.keys(params).forEach((key) => {
      if (!params[key]) { params[key] = '' }
    })

    if (this.state.recipeActionType === "create") {
      this.props.actions.createRecipe({...params})
      .then(() => {
        this.props.history.push('/')
      })
    } else {
      this.props.actions.updateRecipe({
        ...this.state.currentRecipe, 
        ...{'previousTitle': this.state.currentRecipe.title}, 
        ...params
      })
      .then(() => {
        this.props.history.push(`/`)
      })
    }
  }

  isPageURLValid = () => {
    if (this.state.recipeActionType === "create") {
      this.setState({ pageTitle: 'Create brand new recipe' })
      return true
    }
    else if (this.state.recipeActionType === "update") {
      this.setState({ pageTitle: 'Update existing recipe' })
      return true
    }

    this.setState({
      isRequestedPageValid: false
    })
    console.error('Page URL was not recognited')
    return false
  }

  componentDidMount () {
    if (!this.isPageURLValid()) { return }

    if (this.state.recipeActionType === 'update'){
      let recipeId = queryString.parse(this.props.location.search).id
      this.props.actions.loadRecipeById(recipeId)
    }
  }

  componentWillUnmount(){
    this.props.actions.clearCurrentRecipe()
  }

  render () {
    return (
      <div id="recipe-form-page" className="fade-in-animation">
        {!this.state.isRequestedPageValid && <div className="no-data-prompt">{`Url praram '${this.state.recipeActionType}' was not recognited`}</div>}

        {this.state.isRequestedPageValid && <div>
          <h3 className="page-title">{this.state.pageTitle}</h3>
          <div className="recipe-form">
            <div className="form-header">Submission form</div>
            {this.state.isUserFormInvalid && <div className="alert alert-danger">Can not send recipe without title</div>}
            <RecipeForm onSubmit={this.onFormSubmit} key={this.state.currentRecipe.id} 
              currentRecipe={this.state.currentRecipe}/>
          </div>
        </div>}
      </div>
    )
  }
}

RecipeFormPage.propTypes = {
  currentRecipe: PropTypes.object.isRequired,

  match: PropTypes.shape({
    params: PropTypes.shape({
      action: PropTypes.string.isRequired
    })
  })
}

function mapStateToProps (storageState, ownProps) {
  return {
    currentRecipe: storageState.currentRecipe,
  }
}

function mapDispatchToProps (dispatch) {
  return { actions: bindActionCreators({
    ...currentRecipeActions,
    ...recipesAlterationHistoryActions
  }, dispatch) }
}

export default connect(mapStateToProps, mapDispatchToProps)(RecipeFormPage)
