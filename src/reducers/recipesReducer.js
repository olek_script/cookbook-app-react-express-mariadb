import * as types from '../actions/actionTypes'
import initialState from './initialState'

export default function recipesReducer (state = initialState.recipesList, action) {
  switch (action.type) {
    case types.LOAD_RECIPES_LIST: {
      return [...action.recipes]
    }
    default:
      return state
  }
}
