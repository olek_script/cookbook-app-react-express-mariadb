import * as types from '../actions/actionTypes'
import initialState from './initialState'

export default function currentRecipeReducer (state = initialState.currentRecipe, action) {
  switch (action.type) {
    case types.LOAD_SINGLE_RECIPE: {
      return {...action.recipe}
    }
    case types.CLEAR_CURRENT_RECIPE: {
      return {}
    }
    default:
      return state
  }
}
