import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'

import recipeItems from './recipesReducer'
import currentRecipe from './currentRecipeReducer'
import currentRecipeVersions from './currentRecipeVersionsReducer'
import recipesAlterationHistory from './recipesAlterationHistoryReducer'

const rootReducer = combineReducers({
  form: formReducer,
  recipeItems,
  currentRecipe,
  currentRecipeVersions,
  recipesAlterationHistory
})

export default rootReducer
