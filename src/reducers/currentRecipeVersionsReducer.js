import * as types from '../actions/actionTypes'
import initialState from './initialState'

export default function currentRecipeVersionsReducer (state = initialState.currentRecipeVersions, action) {
  switch (action.type) {
    case types.LOAD_RECIPE_VERSIONS: {
      return [...action.recipeVersions]
    }
    default:
      return state
  }
}
