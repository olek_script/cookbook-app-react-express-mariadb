export default {
  recipesList: [],
  currentRecipe: {},
  currentRecipeVersions: [],
  recipesAlterationHistory: [] // can be used in report / history component
}
