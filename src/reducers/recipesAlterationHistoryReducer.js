import * as types from '../actions/actionTypes'
import initialState from './initialState'

export default function recipesAlterationHistoryReducer (state = initialState.recipesAlterationHistory, action) {
  switch (action.type) {
    case types.UPDATE_RECIPES_ALTERATION_HISTORY: {
      let messages = [...state]
      messages.push({date: new Date(), text: action.message})
      return messages
    }
    default:
      return state
  }
}
