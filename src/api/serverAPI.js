import axios from 'axios'

let axiosConfigured = axios.create({
  baseURL: 'http://localhost:8080/api/',
  headers: {
    'Content-Type': 'application/json',
  },
  timeout: 5000,
  responseType: 'json', // default
  transformResponse: [function (data) {
    if (Array.isArray(data)) {
      return data.map((element) => {
        if (element.isActual === 0) { element.isActual = false }
        else if (element.isActual === 1) { element.isActual = true }
        return element
      })
    }

    return data
  }]
});

class ServerAPI {
  static getAllRecipes () {
    return axiosConfigured.get(`recipes`)
      .then((response) => {
        if (response.status !== 200) {
          return Promise.reject(`Got unsufficient status: ${response.status}`)
        }
        return response.data
      })
      .catch((error) => {
        console.error(`XML request for all recipes failed: ${error}`)
      })
  }

  static getRecipe (id) {
    return axiosConfigured.get(`recipe?id=${id}`)
      .then((response) => {
        if (response.status !== 200) {
          return Promise.reject(`Got unsufficient status: ${response.status}`)
        }
        return response.data
      })
      .catch((error) => {
        console.error(`XML request for getting recipe failed: ${error}`)
      })
  }

  static getAllRecipeVersions (groupId) {
    return axiosConfigured.get(`recipe/versions?groupId=${groupId}`)
      .then((response) => {
        if (response.status !== 200) {
          return Promise.reject(`Got unsufficient status: ${response.status}`)
        }
        return response.data
      })
      .catch((error) => {
        console.error(`XML request for getting recipe versions failed: ${error}`)
      })
  }

  static addNewRecipe (recipeParams) {
    return axiosConfigured.post(`recipes/add`, {
      title: recipeParams.title,
      description: recipeParams.description
    })
    .then((response) => {
      if (response.status !== 200) {
        return Promise.reject(`Got unsufficient status: ${response.status}`)
      }
      return response.data
    })
    .catch((error) => {
      console.error(`XML request for adding new recipe failed: ${error}`)
    })
  }

  static updateRecipe (recipe) {
    return axiosConfigured.patch(`recipes/update`, {...recipe})
    .then((response) => {
      if (response.status !== 200) {
        return Promise.reject(`Got unsufficient status: ${response.status}`)
      }
      return response.data
    })
    .catch((error) => {
      console.error(`XML request for updating recipe failed: ${error}`)
    })
  }

  static deleteRecipesGroup (params) {
    return axiosConfigured.delete(`recipes/delete`, {
      data: { // axios.delete not accepts body for 'delete', so used config.data
        groupId: params.groupId,
        isActual: params.isActual,
        title: params.title
      }
    })
    .then((response) => {
      if (response.status !== 200) {
        return Promise.reject(`Got unsufficient status: ${response.status}`)
      }
      return response.data
    })
    .catch((error) => {
      console.error(`XML request for deleting recipe group failed: ${error}`)
    })
  }
}

export default ServerAPI
