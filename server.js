const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')
const config = require('./src/server/config')
require('./src/server/DB/mariaUtil').createDbConnectionPool()

const app = express()
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', config.allowOriginAddress);
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  res.header('Access-Control-Allow-Credentials', true);
  next();
})
app.use(express.static(path.join(__dirname, config.buildFolder)))

require('./src/server/API/main')(app, path)

app.use(function(err, req, res, next) {
  console.error(err.stack); // write to error logger, file, etc..
});

app.listen(config.port, () => {
  console.log(`Server running on port ${config.port}`)
})